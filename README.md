# Carteira de investimentos

## Descrição:

A ideia desse exercício é simular uma carteira de investimentos, para isso teremos:

 - Um CRUD de cliente
 - Um CRUD de produtos
 - Um ambiente de simulação de investimento
 - Um lugar onde será possivel fazer uma aplicação em um investimento

 ## Regras:

 - Workflow de feature branch **obrigatório**
 - Uma PR só pode ser aprovada, *se* os testes da feature estiverem presentes
 - Minimo de coverage: **70%**
 - Limite de pessoas por squad: **2**

 ## Extras:
 ### Calculo do valor final de uma aplicação:
 ```java
    double valorFinal = Math.pow((rendimento + 1), meses) * valor;
 ```

___
 ## Requisições de produto:

 ### GET /produto
Exibe a lista de produtos no sistema.

**Response 200**
```json
[
    {
        "id": 1,
        "nome": "Poupança",
        "rendimento": 0.005
    },
    {
        "id": 2,
        "nome": "CDB",
        "rendimento": 0.007
    },
    {
        "id": 3,
        "nome": "Fundos",
        "rendimento": 0.008
    }
]
```

 ### POST /produto
Cria um produto no sistema.

**Request Body**
```json
{
    "nome": "Poupança",
    "rendimento": 0.005
}
```

**Response 200**
```json
{
    "id": 1,
    "nome": "Poupança",
    "rendimento": 0.005
}
```

### PUT /produto/{id}
Edita um produto do sistema.

**Request Body**
```json
{
    "nome": "Nova-Poupança",
    "rendimento": 0.006
}
```

**Response 200**
```json
{
    "id": 1,
    "nome": "Nova-Poupança",
    "rendimento": 0.006
}
```

### DELETE /produto/{id}
Apaga um produto do sistema.

**Response 204**

___
 ## Requisições de cliente:

 ### GET /cliente
Exibe a lista de clientes no sistema.

**Response 200**
```json
[
    {
        "id": 1,
        "nome": "Josue Antonio",
        "cpf": "311.192.960-48"
    },    
    {
        "id": 2,
        "nome": "Roberto Medeiros",
        "cpf": "126.352.990-99"
    },
    {
        "id": 3,
        "nome": "Carlos de Almeida",
        "cpf": "426.095.960-30"
    },
]
```

 ### POST /cliente
Cria um cliente no sistema.

**Request Body**
```json
{
    "nome": "Carlos de Almeida",
    "cpf": "426.095.960-30"
}
```

**Response 200**
```json
{
    "id": 3,
    "nome": "Carlos de Almeida",
    "cpf": "426.095.960-30"
}
```

### PATCH /cliente/{id}
Edita somente o nome do cliente no sistema.

**Request Body**
```json
{
    "nome": "Almeida Carlos"
}
```

**Response 200**
```json
{
    "id": 3,
    "nome": "Almeida Carlos",
    "cpf": "426.095.960-30"
}
```

### DELETE /cliente/{id}
Apaga um cliente do sistema.

**Response 204**

___
 ## Requisições de aplicação:

 ### GET /cliente/{id}/aplicacao
Exibe as aplicações de um cliente

**Response 200**
```json
[
    {
        "id": 1,
        "valor": 1000.00,
        "meses": 24,
        "produto": {
            "id": 1,
            "nome": "Poupança",
            "rendimento": 0.005
        }
    },    
    {
        "id": 2,
        "valor": 5000.00,
        "meses": 12,
        "produto": {
            "id": 2,
            "nome": "CDB",
            "rendimento": 0.007
        }
    }
]
```

 ### POST /cliente/{id}/aplicacao
Cria uma aplicação para um cliente

**Request Body**
```json
{
    "valor": 1000.00,
    "meses": 24,
    "produto": {
        "id": 1
    }
}
```

**Response 200**
```json
{
    "id": 1,
    "valor": 1000.00,
    "meses": 24,
    "produto": {
        "id": 1,
        "nome": "Poupança",
        "rendimento": 0.005
    }
}
```
___
 ## Requisições de simulação:

 ### POST /simulacao
Simula uma aplicação

**Request Body**
```json
{
    "valor": 1000.00,
    "meses": 24,
    "produto": {
        "id": 1
    }
}
```

**Response 200**
```json
{
    "valorFinal": 1127.15
}
```
