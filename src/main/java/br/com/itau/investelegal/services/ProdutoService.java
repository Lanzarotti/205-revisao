package br.com.itau.investelegal.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import br.com.itau.investelegal.models.Produto;
import br.com.itau.investelegal.repositories.ProdutoRepository;


@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public Iterable<Produto> obterProdutos() {

		return produtoRepository.findAll();
	}

	public Produto criarProduto(Produto produto) {
		return produtoRepository.save(produto);
	}

	public void apagarProduto(Long id) {
		Produto produto = encontraOuDaErro(id);
		produtoRepository.delete(produto);
	}

	public Produto editarProduto(Long id, Produto produtoAtualizado) {
		Produto produto = encontraOuDaErro(id);

		return produtoRepository.save(produto);
	}

	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}

		return optional.get();
	}

}
