package br.com.itau.investelegal.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.investelegal.models.Cliente;
import br.com.itau.investelegal.models.ClienteNomeDTO;
import br.com.itau.investelegal.repositories.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Iterable<Cliente> obterClientes() {
		return clienteRepository.findAll();

	}

	public Cliente criarCliente(Cliente cliente) {
		clienteRepository.save(cliente);
		return cliente;
	}

	
	public void apagarCliente(Long id) {
		Cliente cliente = encontraOuDaErro(id);
		clienteRepository.delete(cliente);
	}

	public Cliente editarCliente(Long id, ClienteNomeDTO clienteAtualizado) {
		Cliente cliente = encontraOuDaErro(id);
		cliente.setNome(clienteAtualizado.getNome());
		return clienteRepository.save(cliente);
	}

	public Cliente encontraOuDaErro(long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}
		return optional.get();
	}
}
