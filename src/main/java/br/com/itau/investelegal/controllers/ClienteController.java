package br.com.itau.investelegal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investelegal.models.Cliente;
import br.com.itau.investelegal.models.ClienteNomeDTO;
import br.com.itau.investelegal.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	

	@Autowired
	private ClienteService clienteService;

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Iterable<Cliente> listarCliente() {
		return clienteService.obterClientes();
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Cliente criarCliente(@RequestBody Cliente clientes) {
		return clienteService.criarCliente(clientes);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarCliente(@PathVariable Long id) {
		clienteService.apagarCliente(id);
	}

//	@PutMapping("/{id}")
//	@ResponseStatus(code = HttpStatus.OK)
//	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
//		return clienteService.editarCliente(id, cliente);
//	}
	
	@PatchMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Cliente editarCliente(@PathVariable Long id, @RequestBody ClienteNomeDTO cliente) {
	return clienteService.editarCliente(id, cliente);
}
	
}
