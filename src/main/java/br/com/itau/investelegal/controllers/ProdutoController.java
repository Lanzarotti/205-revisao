package br.com.itau.investelegal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.itau.investelegal.models.Produto;
import br.com.itau.investelegal.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	private ProdutoService produtoServices;

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Iterable<Produto> listarProdutos() {
		return produtoServices.obterProdutos();
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Produto criarProduto(@RequestBody Produto produto) {
		return produtoServices.criarProduto(produto);
	}

	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Produto editarProduto(@PathVariable Long id, @RequestBody Produto produto) {
		return produtoServices.editarProduto(id, produto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarFilme(@PathVariable Long id) {
		produtoServices.apagarProduto(id);
	}

}
