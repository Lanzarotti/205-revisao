package br.com.itau.investelegal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestelegalApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvestelegalApplication.class, args);
	}

}

