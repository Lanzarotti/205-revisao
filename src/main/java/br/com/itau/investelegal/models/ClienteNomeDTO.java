package br.com.itau.investelegal.models;

public class ClienteNomeDTO {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
