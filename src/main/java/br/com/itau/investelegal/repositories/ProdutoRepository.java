package br.com.itau.investelegal.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.itau.investelegal.models.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long> {

}