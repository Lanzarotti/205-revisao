package br.com.itau.investelegal.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.investelegal.models.Cliente;
import br.com.itau.investelegal.services.ClienteService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { ClienteController.class })

public class ClienteControllerTest {

	@MockBean
	ClienteService clienteService;

	@Autowired
	MockMvc mockMvc;

	@Test
	public void deveObterListaDeClientes() throws Exception {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Gatinho");

		List<Cliente> clientes = Lists.newArrayList(cliente);

		Mockito.when(clienteService.obterClientes()).thenReturn(clientes);
		mockMvc.perform(get("/cliente")).andExpect(status().isOk())
				.andExpect(content().string(CoreMatchers.containsString("1")));
	}

	@Test
	public void deveCriarumCliente() throws JsonProcessingException, Exception {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Chapolim");

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(post("/cliente").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(cliente))).andExpect(MockMvcResultMatchers.status().isOk());

		Mockito.verify(clienteService).criarCliente(Mockito.any(Cliente.class));

	}

//	@Test
//	public void deveEditarumCliente() throws JsonProcessingException, Exception {
//		Cliente cliente = new Cliente();
//		cliente.setId(1L);
//		cliente.setNome("Chaves");
//
//		ObjectMapper objectMapper = new ObjectMapper();
//
//		mockMvc.perform(put("/cliente/1").contentType(MediaType.APPLICATION_JSON_UTF8)
//				.content(objectMapper.writeValueAsString(cliente)))
//		.andExpect(MockMvcResultMatchers.status().isOk());

		//Mockito.verify(clienteService).editarCliente(Mockito.eq(cliente.getId()), Mockito.any(Cliente.class));

//	}
	
	@Test
	public void deveExcluirUmCliente() throws JsonProcessingException, Exception {
	

		mockMvc.perform(delete("/cliente/1"))
		.andExpect(MockMvcResultMatchers.status().isNoContent());

		Mockito.verify(clienteService).apagarCliente(1L);

	}
	
	@Test
	public void deveEditarumNomeCliente() throws JsonProcessingException, Exception {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Chaves");

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(patch("/cliente/1").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(cliente)))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}


}
