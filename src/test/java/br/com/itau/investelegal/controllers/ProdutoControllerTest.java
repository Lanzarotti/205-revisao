package br.com.itau.investelegal.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.List;
import org.assertj.core.util.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.investelegal.models.Produto;
import br.com.itau.investelegal.services.ProdutoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { ProdutoController.class })
public class ProdutoControllerTest {

	@MockBean
	private ProdutoService produtoService;
	
	@Autowired
	private MockMvc mockMvc;

	private Produto criarProduto(Long id) {
		Produto produto = new Produto(id);
		produto.setNome("Poupança");
		produto.setRendimento(0.01);
		return produto;
	}

	@Test
	public void deveObeterListaDeProdutos() throws Exception {
		Produto produto = criarProduto(1L);
		List<Produto> filmes = Lists.newArrayList(produto);
		Mockito.when(produtoService.obterProdutos()).thenReturn(filmes);
		mockMvc.perform(get("/produto")).andExpect(status().isOk())
				.andExpect(content().string(CoreMatchers.containsString("1")));
	}

	@Test
	public void deveCriarumProduto() throws JsonProcessingException, Exception {
		Produto produto = criarProduto(1L);
		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(post("/produto").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(produto))).andExpect(MockMvcResultMatchers.status().isOk());

		Mockito.verify(produtoService).criarProduto(Mockito.any(Produto.class));
	}

	@Test
	public void deveEditarumProduto() throws JsonProcessingException, Exception {
		Produto produto = criarProduto(1L);
		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(put("/produto/1").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(produto))).andExpect(MockMvcResultMatchers.status().isOk());

		Mockito.verify(produtoService).editarProduto(Mockito.eq(produto.getId()), Mockito.any(Produto.class));
	}

	@Test
	public void deveExcluirumProduto() throws JsonProcessingException, Exception {

		mockMvc.perform(delete("/produto/1"))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

		Mockito.verify(produtoService).apagarProduto(1L);
	}

}
