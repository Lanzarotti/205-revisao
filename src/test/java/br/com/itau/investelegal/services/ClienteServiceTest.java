package br.com.itau.investelegal.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.investelegal.models.Cliente;
import br.com.itau.investelegal.models.ClienteNomeDTO;
import br.com.itau.investelegal.repositories.ClienteRepository;

@RunWith(SpringRunner.class)
//@SpringBootTest
@ContextConfiguration(classes = { ClienteService.class })

public class ClienteServiceTest {

	@Autowired
	ClienteService clienteService;
	// TODO Auto-generated constructor s

	@MockBean
	ClienteRepository ClienteRepository;

	@Test
	public void deveListarTodosOsClientes() {

		// given
		Cliente cliente = new Cliente();

		List<Cliente> clientes = new ArrayList<>();
		clientes.add(cliente);

		when(ClienteRepository.findAll()).thenReturn(clientes);

		// action
		Iterable<Cliente> resultado = clienteService.obterClientes();

		// check
		Iterator<Cliente> iterador = resultado.iterator();
		Cliente clienteResultado = iterador.next();

		// ArrayList<Filme> filmeList = Lists.newArrayList(filmes);
		// assertEquals(1, filmeList.size());

		assertNotNull(clienteResultado);

//		// assertNotNull(filmes);
//		int contador = 0;
//		for (Filme filme : filmes) {
//			contador++;
	}

	@Test
	public void deveCriarCliente() {

		// setup
		Cliente cliente = new Cliente();

		//
		clienteService.criarCliente(cliente);

		// check

		Mockito.verify(ClienteRepository).save(cliente);

	}

	@Test
	public void deveEditarUmCliente() {

		// setup
		long id = 1L;
		String novoNome = "Paulo";
		String novoCPF = "323.490.558-41";
		Cliente clienteDoBanco = criarCliente(1L);
		Cliente clienteDoFront = criarCliente(1L);
		clienteDoFront.setNome("José Carlos Lanzarotti");

		ClienteNomeDTO clienteNomeDTO = new ClienteNomeDTO();
		clienteNomeDTO.setNome("Paulo");

		when(ClienteRepository.findById(id)).thenReturn(Optional.of(clienteDoBanco));
		when(ClienteRepository.save(clienteDoBanco)).thenReturn(clienteDoBanco);

		// action
		Cliente resultado = clienteService.editarCliente(id, clienteNomeDTO);

		// check

		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());
		assertNotEquals(novoCPF, resultado.getCpf());

	}

	private Cliente criarCliente(Long id) {
		Cliente cliente = new Cliente();
		cliente.setId(id);
		cliente.setNome("José Carlos Lanzarotti Junior");
		cliente.setCpf("000.111.222-33");
		return cliente;

	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarCliente() {
		// setup
		long id = 1L;
		ClienteNomeDTO clienteDoFront = new ClienteNomeDTO();

		when(ClienteRepository.findById(id)).thenReturn(Optional.empty());

		// action
		Cliente resultado = clienteService.editarCliente(id, clienteDoFront);
	}

	@Test
	public void apagarCliente() {
		// setup
		long id = 1L;
		Cliente clienteDoFront = criarCliente(id);
		Cliente clienteDoBanco = criarCliente(id);

		when(ClienteRepository.findById(id)).thenReturn(Optional.of(clienteDoBanco));
		//
		clienteService.apagarCliente(clienteDoFront.getId());

		// check

		verify(ClienteRepository).delete(Mockito.any(Cliente.class));
		assertEquals(clienteDoFront.getId(), clienteDoBanco.getId());

	}

}
