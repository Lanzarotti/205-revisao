package br.com.itau.investelegal.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.investelegal.models.Produto;
import br.com.itau.investelegal.repositories.ProdutoRepository;
import br.com.itau.investelegal.services.ProdutoService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ProdutoService.class })
public class ProdutoServiceTest {

	@Autowired
	ProdutoService produtoService;

	@MockBean
	ProdutoRepository produtoRepository;

	private Produto criarProduto(Long id) {
		Produto produto = new Produto(id);
		produto.setNome("Poupança");
		produto.setRendimento(0.01);
		return produto;
	}

	@Test
	public void deveListarTodosOsProdutos() {

		// setup
		Produto produto = criarProduto(1L);
		List<Produto> filmes = new ArrayList<>();
		filmes.add(produto);

		when(produtoRepository.findAll()).thenReturn(filmes);

		// action
		List<Produto> resultado = Lists.newArrayList(produtoService.obterProdutos());

		// check
		assertNotNull(resultado.get(0));
		assertEquals(produto, resultado.get(0));
	}

	@Test
	public void deveCriarUmProduto() {
		// setup
		Produto produto = criarProduto(1L);

		// action
		produtoService.criarProduto(produto);

		// check
		verify(produtoRepository).save(produto);
	}

	@Test
	public void deveEditarUmProduto() {
		// setup
		Long id = 1L;
		Long novoId = 10L;
		String novoNome = "PIC";
		Produto produtoDoBanco = criarProduto(id);
		Produto produtoDoFront = criarProduto(id);
		produtoDoFront.setId(novoId);
		produtoDoFront.setNome(novoNome);

		when(produtoRepository.findById(1L)).thenReturn(Optional.of(produtoDoBanco));
		when(produtoRepository.save(produtoDoBanco)).thenReturn(produtoDoBanco);

		// action
		Produto resultado = produtoService.editarProduto(id, produtoDoFront);

		// check
		assertNotNull(resultado);
		assertEquals(produtoDoFront.getRendimento(), produtoDoBanco.getRendimento(), 0.001);
		assertNotEquals(produtoDoFront.getId(), resultado.getId());
		assertNotEquals(produtoDoFront.getNome(), resultado.getNome());
	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOProduto() {
		// setup
		Long id = 1L;
		Produto produtoDoFront = criarProduto(id);

		when(produtoRepository.findById(id)).thenReturn(Optional.empty());

		// action
		produtoService.editarProduto(id, produtoDoFront);
	}

	@Test
	public void deveApagarUmProduto() {
		// setup
		Long id = 1L;
		Produto produtoDoFront = criarProduto(id);
		Produto produtoDoBack = criarProduto(id);
		when(produtoRepository.findById(1L)).thenReturn(Optional.of(produtoDoBack));

		// action
		produtoService.apagarProduto(produtoDoFront.getId());

		// check
		verify(produtoRepository).delete(Mockito.any(Produto.class));
		assertEquals(produtoDoFront.getId(), produtoDoBack.getId());
	}

}
